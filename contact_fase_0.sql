-- Table: public.contact

DROP TABLE IF EXISTS public.contact_fase_0;

CREATE TABLE public.contact_fase_0
(
    "idContact" serial,
    "name" text NOT NULL,
    email text NOT NULL,
    phone text NOT NULL,
    topic text NOT NULL,
    "message" text NOT NULL,
    "createTime" timestamp DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT contact_pkey PRIMARY KEY ("idContact")
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.contact_fase_0
    OWNER to pymefectivo;
COMMENT ON TABLE public.contact_fase_0
    IS 'tabla para guardar info de la pantalla contacto, donde se reportan problemas o se envian dudas y sugerencias';