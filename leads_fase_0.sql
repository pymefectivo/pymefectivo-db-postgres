-- Table: public.leads_fase_0

DROP TABLE IF EXISTS public.leads_fase_0;

CREATE TABLE public.leads_fase_0
(
    id_leads serial,
    name text COLLATE pg_catalog."default" NOT NULL,
    email text COLLATE pg_catalog."default" NOT NULL,
    phone text COLLATE pg_catalog."default" NOT NULL,
    postal_code text COLLATE pg_catalog."default" NOT NULL,
    create_time timestamp DEFAULT CURRENT_TIMESTAMP,
    update_time timestamp DEFAULT CURRENT_TIMESTAMP,
    status text DEFAULT '' COLLATE pg_catalog."default",
    CONSTRAINT leads_fase_0_pkey PRIMARY KEY (id_leads)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.leads_fase_0
    OWNER to pymefectivo;
COMMENT ON TABLE public.leads_fase_0
    IS 'tabla para los leads fase 0';